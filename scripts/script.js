//
// drawScene
//
// Draw the scene.
//
function drawScene() {
    // set the rendering environment to full canvas size
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    // Clear the canvas before we start drawing on it.
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Establish the perspective with which we want to view the
    // scene. Our field of view is 45 degrees, with a width/height
    // ratio and we only want to see objects between 0.1 units
    // and 100 units away from the camera.
    mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0, pMatrix);
    mat4.translate(pMatrix, [0, -4, -110]);

    scene.draw();

    for (var i = obstacleMap.length - 1; i >= 0; i--){
		var obstacleList = obstacleMap[i].obstacles;
        for (var j = 0; j < obstacleList.length; j++){
            var o = obstacleList[j];
            var type = o.type;
            var x = o.posX, y = o.posY, z = o.posZ; 
            
            //console.log("Type: "+type+", pos: ["+x+","+y+","+z+"]")
            if(o.scared){

                
                if(!o.scaredProgress){
                    o.scaredProgress = 0.0;
                }
                o.scaredProgress += 0.1;
                
                obstacleTypes[type].draw([x,y,z], o.scaredProgress);

                // Do away with the grizzly!
                if(o.scaredProgress >= 1){
                    clearArray(obstacleMap[i].segmentGrid);
				    obstacleMap[i].obstacles = [];
                }


            }else {
                obstacleTypes[type].draw([x,y,z]);
            }

            
            
        }
    }
    yodler.draw();
}


function printObjectMap(map){

    map = obstacleMap;

    for(var i = 0; i < map.length; i++){
        var s = i+": ", p = false;
        for(var j = 0; j < map[i].length; j++){
            s += "["+map[i][j].posX+","+map[i][j].posY+","+map[i][j].posZ+"] ";
            p = true;
        }
        if(p)
            console.log(s);
    }
}

function addNewObstacles(kk){
	//console.log("Added obstacle!");
    if(kk)
        obstacleMap.shift()
		
        var s = generateSegment()
    for (var u = 0; u < s.obstacles.length; u++){
        s.obstacles[u].posZ = renderDistance;
    }
    obstacleMap.push(s);
}

function removePickedUpCoin() {
	var obstacles = obstacleMap[0].obstacles;

	if (obstacles[0].type === "coin" && playerXPosition/5+1 === obstacles[0].posX) {
		obstacleMap[0].obstacles.splice(0, 1);
	}
}

//
// animate
//
// Called every time before redrawing the screen.
//



function animate() {

    var timeNow = new Date().getTime();
    if (lastTime != 0) {
        var elapsed = timeNow - lastTime;

        // update coin rotation
		rotationCoin += (rotationVelocity * elapsed) / 1000.0;
        
        // Move obstacles forward
		// Iterate over segments
		for (var segNum = 0; segNum < obstacleMap.length; segNum++){
			var obsts = obstacleMap[segNum].obstacles;
			obstacleMap[segNum].segmentPosZ -= runnerSpeed/30;
			// if segment is behind yodler, remove segment
			if (obstacleMap[segNum].segmentPosZ < -0.5){
				addNewObstacles(true);
			}
			else {
				// if segment is in front of yodler, move segment closer
				for (var obstNum = 0; obstNum < obsts.length; obstNum++){
					obsts[obstNum].posZ -= runnerSpeed/30;
				}
			}
		}
		
		if (currentSegmentID != obstacleMap[0].id){
			if (slideInProgress){
				slideSegCounter--;
            }
            
            var crash = false;
            var coinPicked = false;
			
			// check for collision
			var curSegGrid = obstacleMap[0].segmentGrid;
			if (obstacleMap[0].obstacles.length > 0){
				if (jumpInProgress){
					if (curSegGrid[0][playerXPosition/5 + 1] == 1 || curSegGrid[1][playerXPosition/5 + 1] == 1){
						console.log("Crashed into:");
						console.log(obstacleMap[0].obstacles);
						crash = true;
					}
				}
				else if (slideInProgress){
					if (curSegGrid[2][playerXPosition/5 + 1] == 1){
						console.log("Crashed into:");
						console.log(obstacleMap[0].obstacles);
						crash = true;						
					}
				}
				else{
					if (curSegGrid[2][playerXPosition/5 + 1] == 1 || curSegGrid[1][playerXPosition/5 + 1] == 1){
						console.log("Crashed into:");
						console.log(obstacleMap[0].obstacles);
						crash = true;
					}	
				}
				
				if (jumpInProgress){
					if (curSegGrid[0][playerXPosition/5 + 1] == 2 || curSegGrid[1][playerXPosition/5 + 1] == 2){
						console.log("Picked up coin!");
						coinPicked = true;
					}
				}
				else if (slideInProgress){
					if (curSegGrid[2][playerXPosition/5 + 1] == 2){
						console.log("Picked up coin!");
						coinPicked = true;						
					}
				}
				else{
					if (curSegGrid[2][playerXPosition/5 + 1] == 2 || curSegGrid[1][playerXPosition/5 + 1] == 2){
						console.log("Picked up coin!");
						coinPicked = true;
					}	
				}
            }
            
            if(crash){
                playerCrashedSound();
                hitStatus.playerHit = true;
            }

            if(coinPicked){
                removePickedUpCoin();
                addScore(1);
                coinsound();
                coinPicked = false;
            }
			
			currentSegmentID = obstacleMap[0].id;
//			console.log("updating currentSegmentID");
			if (slideInProgress && slideSegCounter <= 0){
				//console.log("Ending slide");
				slideInProgress = false;
			}
		}

        // Generate new obstacles
        counter += step;

        if (counter >= 2) {
            
            addScore(2);

        //    addNewObstacles(true)
            counter = 0;
        }
        
    }
    lastTime = timeNow;
}

function startDrawing(){
    // Set up to draw the scene periodically.
    drawingInterval = setInterval(function() {
    	if (texturesLoaded === numberOfTextures) {
            if(!hitStatus.playerHit)
            	requestAnimationFrame(animate);
            handleKeys();
            drawScene();
        }
    }, 33);
}

function stopDrawing() {
    clearInterval(drawingInterval);
}

var obstacleMap = [];

function init_generate_obstacles(){
    for(var i = 0; i < renderDistance-10; i++){
		var segment;
		if (i < 10){
			segment = generateEmptySegment();
		}
		else{
			segment = generateSegment();
		}
		segment.segmentPosZ = i;
        for( var j = 0; j < segment.obstacles.length; j++){
            var o = segment.obstacles[j];
            var type = o.type;
            var x = o.posX, y = o.posY; 
            var z = i;

            //console.log("Type: "+type+", pos: ["+x+","+y+","+z+"]")
            
            o.posZ = z;
            
        }
        obstacleMap.push(segment);
    }
	for (var i = 0; i < obstacleMap.length; i++){
		//console.log(obstacleMap[i].id);
	}
}

function start() {
    canvas = document.getElementById("glcanvas");

	// Handle window resize
	window.onresize = handleResize;
	handleResize();
	
    gl = initGL(canvas);      // Initialize the GL context
    if (gl) {
        initShaders();
        yodler = new YodlerObject();
        scene = new SceneObject();
        obstacleTypes = {
            "rock": new RockObstacle(),
            "tree": new TreeObstacle(),
            "branch": new BranchObstacle(),
            "overhang": new OverhangObstacle(),
            "grizzly": new GrizzlyObstacle(),
			"coin": new CoinObstacle(),
        };
        initBuffers();
        initTextures();
        loadJSONModels();

        document.onkeydown = handleKeyDown;

        $(".sound_setting").click(function() {

            //alert("HALO?")
            toggleSound()

        });

        // Odkomentiraj, ko razvijaš igro, da se ti ne prikazujejo okna...
        //startGame();
    }
}
