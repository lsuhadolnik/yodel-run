function projectColors() {

    this.blue   = [
        62.0/255.0, 100.0/255.0, 119.0/255.0, 1.0
    ];

    this.orange = [
        254.0/255.0, 127.0/255.0, 46.0/255.0, 1.0
    ];

    this.yellow = [
        252.0/255.0, 202.0/255.0, 71.0/255.0, 1.0
    ];

    this.lightGreen = [
        161.0/255.0, 193.0/255.0, 128.0/255.0, 1.0
    ];

    this.darkGreen = [
        135.0/255.0, 156.0/255.0, 135.0/255.0, 1.0
    ];

    this.bearLegs = [
        153.0/255.0, 102.0/255.0, 51.0/255.0, 1.0
    ];

    this.bearHead = this.bearLegs;

    this.bearBody = [
        96.0/255.0, 64.0/255.0, 32.0/255.0, 1.0
    ];
}

var projColors = new projectColors();
