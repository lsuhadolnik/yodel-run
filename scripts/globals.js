// Global variable definitionvar canvas;
var gl;
var shaderProgram;
var canvas;

// Model-view and projection matrix and model-view matrix stack
var mvMatrixStack = [];
var mvMatrix = mat4.create();
var pMatrix = mat4.create();

// The event loop interval
var drawingInterval = null;

// Main game objects
var yodler;
var scene;
var obstacleTypes;

// Obstacle rendering
var renderDistance = 40;
var step = 0.2;
var counter = 0;
    
var currentSegmentID = 0;
// How fast is the game happening
var runnerSpeed = 5; // segments/second
var defaultRunnerSpeed = 5;

// Absolute Milliseconds when the game started
var startingTime = 0;

// The interval with a function which increases the game speed
var gameSpeedIncreaseInterval;

var obstacles;

// Keyboard handling helper variable for reading the status of keys
var currentlyPressedKeys = {};

// Helper variable for animation
var lastTime = 0;

// player position
const startPlayerXPosition = 0.0;
const startPlayerYPosition = 1.0;
var playerXPosition = 0.0;
var playerYPosition = 1.0;
var playerHeight = 2;
var startPlayerHeight = 2;

// game parameters
var gameRunning = false;
var score = 0;
var hiScore = 0;

var moveInProgress = false;
var moveDirection = 0;
var moveProgress = 0;
var horizontalVelocity = 0.25;
var moveDistance = 5;

var verticalVelocity = 0.05;
var jumpProgress = 0.0;
var jumpInProgress = false;
var topOfJumpY = 5.0;

var slideInProgress = false;
var slideSegCounter = 0;

// Variable for storing textures
var grassTexture;
var pathTexture;
var cloudTexture;
var rockTexture;
var treeTrunkTexture;
var treeTopTexture;
var branchTexture;
var overhangTexture;
var grizzlyTexture;
var goldTexture;

// Variable that stores  loading state of textures.
var numberOfTextures = 10;
var texturesLoaded = 0;

var whiteColor = new Float32Array([1, 1, 1, 1]);
var whiteTexture;
var whitePixel;

var teapotVertexNormalBuffer;
var teapotVertexTextureCoordBuffer;
var teapotVertexPositionBuffer;
var teapotVertexIndexBuffer;

var coinVertexNormalBuffer;
var coinVertexTextureCoordBuffer;
var coinVertexPositionBuffer;
var coinVertexIndexBuffer;

var lastTime = 0;
var rotationCoin = 0;
var rotationVelocity = 50;

var hitStatus = new (function(){
    this.playerHit = false;
    this.fallProgress = 0;
    this.fallSpeed = 0.05;
    this.fallAngle = degToRad(90);

    this.reset = function() {
        this.playerHit = false;
        this.fallProgress = 0;
    }

});