
var runway = [];
var blankCounter = 0;
var difficulty = 5;
var segmentID = 0;
    
function updateRunway() {
    runway.shift();
    
};

function generateEmptySegment(){
	var newSegmentData = {
        segmentGrid:[
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0]
                ],
        obstacles:[],
		segmentPosZ: renderDistance,
		id: segmentID
    };
    segmentID++;
	return newSegmentData;
}

function generateSegment(){
    // Blank new segment with no obstacles
    var newSegmentData = {
        segmentGrid:[
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0]
                ],
        obstacles:[],
		segmentPosZ: renderDistance,
		id: segmentID
    };
    segmentID++;    
    if (blankCounter < difficulty) {
		addCoin(newSegmentData);
        blankCounter++;
    }
    else {
        // 90% chance to add first obstacle
        if (Math.random() > 0.1){
            addObstacle(newSegmentData);
            // 66% chance to add another obstacle
            if (Math.random() > 0.33){
                addObstacle(newSegmentData);
            }
        }
        blankCounter = 0;
    }
    return newSegmentData;
}
// obstacle = { type:"rock", posY:2, posX:0, height:1, width:1 } <- places a 1x1 rock in bottom left corner
// Adds obstacle to given "segmentData.segmentGrid", "segmentData.obstacles" contains data of existing obstacle

/*
    CURRENT PROBABILITIES:
        Rock - 45%
        Tree - 20%
        Branch - 15%
        Overhang - 15%
        Grizzly - 5%
*/

function addBuff(segmentData){
	var buffX = Math.ceil(Math.rand()*3);
	var buffY = Math.ceil(Math.rand()*2);
	
}

function addCoin(segmentData){
	var lastSegment = obstacleMap[obstacleMap.length-1];
	var rand = Math.random();
	var coinY = 1;
	var coinX = 1;
	// If previous segment has obstacle, randomly place a coin
	if (lastSegment.obstacles.length > 0 && lastSegment.obstacles[0].type != "coin"){
		if (rand < 0.33){
			coinX -= 1;
		}
		else if (rand > 0.66){
			coinX += 1;
		}
	}
	// If previous segment contains a coin, add another coin with a random horizontal offset
	else if (lastSegment.obstacles.length > 0 && lastSegment.obstacles[0].type === "coin"){
		coinX = lastSegment.obstacles[0].posX;
		if (rand < 0.33 && coinX > 0){
			coinX -= 1;
		}
		else if (rand > 0.66 && coinX < 2){
			coinX += 1;
		}
	}
	segmentData.obstacles.push(getObstacle("coin", coinY, coinX, 1, 1));
	segmentData.segmentGrid[coinY][coinX] = 2;
}

function addObstacle(segmentData){
    
    // If no obstacle exists, add a random one
    if (segmentData.obstacles.length == 0){
        var rand = Math.random();
        if (rand > 0.95){
            // Grizzly
            fillArray(segmentData.segmentGrid, [2,2, 2,1, 2,0, 1,2, 1,1, 1,0, 0,2, 0,1, 0,0]);
            segmentData.obstacles.push(getObstacle("grizzly", 2, 0, 3, 3));
        }
        else if (rand > 0.8){
            // Overhang
            fillArray(segmentData.segmentGrid, [1,2, 1,1, 1,0, 0,2, 0,1, 0,0]);
            segmentData.obstacles.push(getObstacle("overhang", 1, 1, 2, 3));
        }
        else if (rand > 0.65){
            // Branch
            var xPos = Math.ceil(rand*100)%2;
            
            fillArray(segmentData.segmentGrid, [1,xPos, 1,xPos+1]);
            segmentData.obstacles.push(getObstacle("branch", 1, xPos, 1, 2));
        }
        else if (rand > 0.45){
            // Tree
            var xPos = Math.ceil(rand*100)%3;
            
            fillArray(segmentData.segmentGrid, [2,xPos, 1,xPos, 0,xPos]);
            segmentData.obstacles.push(getObstacle("tree", 2, xPos, 3, 1));
        }
        else {
            // Rock
            var xPos = Math.ceil(rand*100)%3;
            
            fillArray(segmentData.segmentGrid, [2,xPos]);
            segmentData.obstacles.push(getObstacle("rock", 2, xPos, 1, 1));
        }
    }
    // If one already exists, add another that fits
    else {
        var rand = Math.random();
        var obstacleX = segmentData.obstacles[0].posX;
        switch (segmentData.obstacles[0].type) {
            case 'rock':
                // Add a Tree
                var xPos = (obstacleX + 1 + Math.ceil(rand*100)%2)%3;
                
                fillArray(segmentData.segmentGrid, [2,xPos, 1,xPos, 0,xPos]);
                segmentData.obstacles.push(getObstacle("tree", 2, xPos, 3, 1));
                break;
            case 'branch':
            case 'overhang':
                // Add a Rock
                var xPos = (obstacleX + Math.ceil(1+rand*100)%2)%3;
                
                fillArray(segmentData.segmentGrid, [2,xPos]);
                segmentData.obstacles.push(getObstacle("rock", 2, xPos, 1, 1));
                break;
            case 'tree':
                // Add a Branch if tree is at 0 or 2
                if (obstacleX != 1){
                    var xPos = (obstacleX+1)%3;
                    fillArray(segmentData.segmentGrid, [1,xPos, 1,xPos+1]);
                    segmentData.obstacles.push(getObstacle("branch", 1, xPos, 1, 2));
                }
                break;
            default:
                // code
        }
    }
    
}

// Generates and return a specified obstacle
function getObstacle(tp, psY, psX, hght, wdth){
    return { type:tp, posY:psY, posX:psX, heght:hght, width:wdth };
}

// Fills in specified spaces in the array with "1"
function fillArray(array, spaces){
    for (var i = 0; i < spaces.length; i+=2){
        array[spaces[i]][spaces[i+1]] = 1;
    }
}

function clearArray(array){
	for (var i = 0; i < array.length; i++){
		for (var j = 0; j < array[i].length; j++){
			array[i][j] = 0;
		}
	}
}

function MEMEengine(){
    var segmentData = generateSegment();
    console.log(segmentData.segmentGrid);
    console.log("----------------------------------------------")
    console.log(segmentData.obstacles);
    console.log("//////////////////////////////////////////////");
}

