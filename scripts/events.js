function handleResize(event) {
	
	canvas.height = window.innerHeight;
    canvas.width = window.innerWidth;
    
    if(event){
        gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
	
    }
    
}


function handleKeyDown(event) {
    // storing the pressed state for individual key
    currentlyPressedKeys[event.keyCode] = true;
}

function handleKeys() {
    if (currentlyPressedKeys[37]) {
        // Left cursor key

        if(!moveInProgress && playerXPosition > -5){
            moveInProgress = true;
            moveDirection = -1;
            moveProgress = 0;
        }

        //playerXPosition = Math.max(-5, playerXPosition - 5);
        currentlyPressedKeys[37] = false;
    }

    if (currentlyPressedKeys[39]) {
        // Right cursor key
        
        if(!moveInProgress && playerXPosition < 5){
            moveInProgress = true;
            moveDirection = 1;
            moveProgress = 0;
        }

        
        //playerXPosition = Math.min(5, playerXPosition + 5);
        currentlyPressedKeys[39] = false;
    }

    if (currentlyPressedKeys[89]) {
        // Right cursor key
        yodelSound();
		currentlyPressedKeys[89] = false;
		// Remove any bears within 10 segments
		for (var i = 1; i <= 11; i++){
			var curSeg = obstacleMap[i];
			if (curSeg.obstacles.length > 0 && curSeg.obstacles[0].type == "grizzly"){

                curSeg.obstacles[0].scared = true;
                console.log("Wolf scared!");

                console.log(curSeg.obstacles[0]);

				//clearArray(curSeg.segmentGrid);
				//curSeg.obstacles = [];
			}
		}
        
    }

    if (currentlyPressedKeys[32]) {
        // Space
        if (playerYPosition === startPlayerYPosition && !jumpInProgress) {
            // start jump only if player is on the ground
            //verticalVelocity = 0.01;
            jumpProgress = 0.0;
            jumpInProgress = true;

            jumpSound();
        }

        currentlyPressedKeys[32] = false;

        /*if (jumpInProgress) {
            if (reachedTopOfJump) {
                // move player down until it touches the ground
                playerYPosition = Math.max(playerYPosition - verticalVelocity, startPlayerYPosition);
            } else {
                // move player up until it reaches the top point
                playerYPosition = Math.min(playerYPosition + verticalVelocity, topOfJumpY);
            }

            if (playerYPosition === topOfJumpY) {
                // player reached top of the jump
                reachedTopOfJump = true;
            }

            if (playerYPosition === startPlayerYPosition) {
                // player touched the ground - end of jump
                reachedTopOfJump = false;
                currentlyPressedKeys[32] = false;
                jumpInProgress = false;
                verticalVelocity = 0.0;
            }
        }*/
    }
	
	if (currentlyPressedKeys[40]) {
        // Down cursor key
		if (!slideInProgress){
			slideInProgress = true;
			slideSegCounter = 3 + Math.floor((runnerSpeed - 5)*0.25);
		}
		currentlyPressedKeys[40] = false;
    }
}

function increaseGameSpeed(){

    var elapsed = (new Date().getTime() - startingTime) / 1000;

    var increaseFunction = function(x) {
        // WTF!? Kaj ti pa je??
        //return 0.023 * Math.pow(x, 2);

        return 0.5*Math.log(Math.pow(x + 1, 4))
    }

    runnerSpeed = defaultRunnerSpeed + increaseFunction(elapsed);

}


function setScore(newScore) {

    score = newScore;
    $(".score_value").text(newScore);

}

function addScore(newScore) {

    score += newScore;
    $(".score_value").text(score);

}

function startGame() {

    if(!soundLoaded){
        alert("Please wait a second... sound is still loading.")
        return;
    }

    startingTime = new Date().getTime();
    runnerSpeed = defaultRunnerSpeed;    


    // Remove overlays
    $("#begin_game_overlay").hide();
    $(".content").removeClass("blurry");

    obstacleMap = [ ];
    playerXPosition = startPlayerXPosition;
    playerYPosition = startPlayerYPosition;
    init_generate_obstacles();

    setScore(0);
    gameRunning = true;

    slideInProgress = false;
    currentlyPressedKeys[40] = false;

    currentlyPressedKeys[32] = false;
    jumpInProgress = false;
    jumpProgress = 0;

    if(hitStatus)
        hitStatus.reset();

    // Starts the main game loop
    startDrawing();

    playGameSound();

    gameSpeedIncreaseInterval = setInterval(increaseGameSpeed, 500);

}

function playerCrashed() {

    stopDrawing();
    gameRunning = false;

    clearInterval(gameSpeedIncreaseInterval);
    
    if(score > hiScore) {
        hiScore = score;
    }

    $(".high_score_value").text(hiScore);

    // Remove overlays
    $("#begin_game_overlay").show();
    
    $("#fresh_game").hide();
    $("#player_crashed").show();

    $(".content").addClass("blurry");

    //playerCrashedSound();

    //startGame()
}


