function BranchObstacle() {
    // Now create an array of vertices for the cube.
	this.branchVertexPositionBuffer = prepareBuffer(new Float32Array([
		// Front face
		-6.0, -1.0,  1.0,
		6.0, -1.0,  1.0,
		6.0,  1.0,  1.0,
		-6.0,  1.0,  1.0,

		// Back face
		-6.0, -1.0, -1.0,
		-6.0,  1.0, -1.0,
		6.0,  1.0, -1.0,
		6.0, -1.0, -1.0,

		// Top face
		-6.0,  1.0, -1.0,
		-6.0,  1.0,  1.0,
		6.0,  1.0,  1.0,
		6.0,  1.0, -1.0,

		// Bottom face
		-6.0, -1.0, -1.0,
		6.0, -1.0, -1.0,
		6.0, -1.0,  1.0,
		-6.0, -1.0,  1.0,

		// Right face
		1.0, -1.0, -1.0,
		1.0,  1.0, -1.0,
		1.0,  1.0,  1.0,
		1.0, -1.0,  1.0,

		// Left face
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,
		-1.0,  1.0,  1.0,
		-1.0,  1.0, -1.0
	]), 3, 24);

	// Now create an array of vertex normals for the cube.
	this.branchVertexNormalBuffer = prepareBuffer(new Float32Array([
		// Front face
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,

		// Back face
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,

		// Top face
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,

		// Bottom face
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,

		// Right face
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,

		// Left face
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0
	]), 3, 24);

	// Now create an array of vertex texture coordinates for the cube.
	this.branchTextureCoordBuffer = prepareBuffer(new Float32Array([
		// Front
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Back
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Top
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Bottom
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Right
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Left
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0
	]), 2, 24);

	this.branchVertexIndexBuffer = prepareBuffer(new Uint16Array([
		0,  1,  2,      0,  2,  3,    // front
		4,  5,  6,      4,  6,  7,    // back
		8,  9,  10,     8,  10, 11,   // top
		12, 13, 14,     12, 14, 15,   // bottom
		16, 17, 18,     16, 18, 19,   // right
		20, 21, 22,     20, 22, 23    // left
	]), 1, 36, gl.ELEMENT_ARRAY_BUFFER);

    this.draw = function(pos) {
        // pos is a position array... [x, y, z]
        mat4.identity(mvMatrix);
        xNorm = (pos[0] - 1) * 5; // Če je 0 vrne -5
        zNorm = 98 - pos[2]*3;

        switch (xNorm) {
            case -5:
                mat4.translate(mvMatrix, [xNorm, 3.0, zNorm]);
                break;
            case 0:
                mat4.translate(mvMatrix, [xNorm+5.0, 3.0, zNorm]);
                break;
            default:
                break;
        }

        mvPushMatrix();

		// Draw the cube by binding the array buffer to the cube's vertices
		// array, setting attributes, and pushing it to GL.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.branchVertexPositionBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.branchVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Set the normals attribute for vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.branchVertexNormalBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.branchVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Set the texture coordinates attribute for the vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.branchTextureCoordBuffer);
		gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.branchTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Activate textures
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, branchTexture);
		gl.uniform1i(shaderProgram.samplerUniform, 0);

		// LIGHTING!
		gl.uniform3f(shaderProgram.ambientColorUniform, 0.7,0.7,0.7);

		var lightingDirection = [-0.25, -0.2, -0.2];

		var adjustedLD = vec3.create();
		vec3.normalize(lightingDirection, adjustedLD);
		vec3.scale(adjustedLD, -1);

		gl.uniform3fv(shaderProgram.lightingDirectionUniform, adjustedLD);

		gl.uniform3f(shaderProgram.directionalColorUniform, 0.8,0.8,0.8);

		gl.uniform4fv(shaderProgram.uColor, whiteColor);  // use white color
		gl.bindTexture(gl.TEXTURE_2D, branchTexture);  // and some texture

		// Draw the cube.
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.branchVertexIndexBuffer);
		setMatrixUniforms();
		gl.drawElements(gl.TRIANGLES, this.branchVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

		mvPopMatrix();
    }
}