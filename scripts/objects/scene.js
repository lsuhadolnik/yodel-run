function SceneObject() {
    /*
        SCENE
     */
    this.sceneVertexPositionBuffer = prepareBuffer(new Float32Array([
        // Bottom face
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, -1.0,  1.0,
        -1.0, -1.0,  1.0,

        // Left face
        -1.0, -1.0, -1.0,
        -1.0, -1.0,  1.0,
        -2.0,  1.0,  1.0,
        -2.0,  1.0, -1.0,

        // Right face
        1.0, -1.0, -1.0,
        2.0,  1.0, -1.0,
        2.0,  1.0,  1.0,
        1.0, -1.0,  1.0,
    ]), 3, 12);

    this.sceneVertexNormalBuffer = prepareBuffer(new Float32Array([
		// Bottom face
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,

		// Left face
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,

		// Right face
		-1.0, 1.0, 0.0,
		-1.0, 1.0, 0.0,
		-1.0, 1.0, 0.0,
		-1.0, 1.0, 0.0,
    ]), 3, 12);

    this.sceneVertexTextureCoordBuffer = prepareBuffer(new Float32Array([
		// Bottom
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Right
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Left
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0
    ]), 2, 12);

    // Katere vrstice v zgornjem Bufferju uporabiti, da izrišem kocko?
    this.sceneVertexIndexBuffer = prepareBuffer(new Uint16Array([
        0, 1, 2,      0, 2, 3,     // Bottom face
        4, 5, 6,      4, 6, 7,     // Left face
        8, 9, 10,     8, 10, 11    // Right face
    ]), 1, 18, gl.ELEMENT_ARRAY_BUFFER);

	/*
		SKY
	 */
    this.skyVertexPositionBuffer = prepareBuffer(new Float32Array([
        // Bottom face
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0, -1.0,  1.0,
        -1.0, -1.0,  1.0,
    ]), 3, 4);

    this.skyVertexNormalBuffer = prepareBuffer(new Float32Array([
		// Front face
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
    ]), 3, 4);

    this.skyVertexTextureCoordBuffer = prepareBuffer(new Float32Array([
		// Front
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
    ]), 2, 4);

    // Katere vrstice v zgornjem Bufferju uporabiti, da izrišem kocko?
    this.skyVertexIndexBuffer = prepareBuffer(new Uint16Array([
        0, 1, 2,      0, 2, 3,    // Front face
    ]), 1, 6, gl.ELEMENT_ARRAY_BUFFER);

	this.drawSky = function(){

		// SKY
        mat4.identity(mvMatrix);
        mat4.translate(mvMatrix, [0, 10, -10]);
        mat4.rotateX(mvMatrix, degToRad(90));
        mat4.scale(mvMatrix, [1000, 100, 100]);
        mvPushMatrix();

        gl.bindBuffer(gl.ARRAY_BUFFER, this.skyVertexPositionBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.skyVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.skyVertexNormalBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.skyVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Set the texture coordinates attribute for the vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.skyVertexTextureCoordBuffer);
		gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.skyVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

		gl.uniform4fv(shaderProgram.uColor, whiteColor);  // use white color
		gl.bindTexture(gl.TEXTURE_2D, cloudTexture);  // and some texture

		setMatrixUniforms();
        gl.drawElements(gl.TRIANGLES, this.skyVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
        mvPopMatrix();
		
	}

	this._push_road_to_gpu = function(){

		gl.bindBuffer(gl.ARRAY_BUFFER, this.sceneVertexPositionBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.sceneVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
		// Set the normals attribute for vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.sceneVertexNormalBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.sceneVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Set the texture coordinates attribute for the vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.sceneVertexTextureCoordBuffer);
		gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.sceneVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Activate textures
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, grassTexture);
		gl.uniform1i(shaderProgram.samplerUniform, 0);

		// LIGHTING!
		gl.uniform3f(shaderProgram.ambientColorUniform, 0.7,0.7,0.7);

		var lightingDirection = [-0.25, -0.2, -0.2];

		var adjustedLD = vec3.create();
		vec3.normalize(lightingDirection, adjustedLD);
		vec3.scale(adjustedLD, -1);

		gl.uniform3fv(shaderProgram.lightingDirectionUniform, adjustedLD);

		gl.uniform3f(shaderProgram.directionalColorUniform, 0.8,0.8,0.8);

		gl.uniform4fv(shaderProgram.uColor, whiteColor);  // use white color
		gl.bindTexture(gl.TEXTURE_2D, pathTexture);  // and some texture

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.sceneVertexIndexBuffer);
        setMatrixUniforms();
        gl.drawElements(gl.TRIANGLES, this.sceneVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
        mvPopMatrix();

	}

	this.drawRoad = function(){

		var t = (new Date().getTime()/100) % 40;

		for(var i = 100; i >= -50; i-= 2) {

			let k = i;
			if(!hitStatus.playerHit)
				k = i + t > 102 ? i + t - 150 : i + t;

			// ROAD
			mat4.identity(mvMatrix);
			mat4.translate(mvMatrix, [0, 4, k]);
			mat4.scale(mvMatrix, [10, 4, 1]);
			mvPushMatrix();
	
			this._push_road_to_gpu()

		}

		

	}

	this.drawSides = function(){

	}

    this.draw = function() {
        
		this.drawRoad();
		this.drawSky();
        
    }
}