function GrizzlyObstacle() {
    // Now create an array of vertices for the cube.
	this.grizzlyVertexPositionBuffer = prepareBuffer(new Float32Array([
		// Front face
		-6.0, -6.0,  1.0,
		6.0, -6.0,  1.0,
		6.0,  6.0,  1.0,
		-6.0,  6.0,  1.0,

		// Back face
		-6.0, -6.0, -1.0,
		-6.0,  6.0, -1.0,
		6.0,  6.0, -1.0,
		6.0, -6.0, -1.0,

		// Top face
		-6.0,  6.0, -1.0,
		-6.0,  6.0,  1.0,
		6.0,  6.0,  1.0,
		6.0,  6.0, -1.0,

		// Bottom face
		-6.0, -6.0, -1.0,
		6.0, -6.0, -1.0,
		6.0, -6.0,  1.0,
		-6.0, -6.0,  1.0,

		// Right face
		6.0, -6.0, -1.0,
		6.0,  6.0, -1.0,
		6.0,  6.0,  1.0,
		6.0, -6.0,  1.0,

		// Left face
		-6.0, -6.0, -1.0,
		-6.0, -6.0,  1.0,
		-6.0,  6.0,  1.0,
		-6.0,  6.0, -1.0
	]), 3, 24);

	// Now create an array of vertex normals for the cube.
	this.grizzlyVertexNormalBuffer = prepareBuffer(new Float32Array([
		// Front face
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,

		// Back face
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,

		// Top face
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,

		// Bottom face
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,

		// Right face
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,

		// Left face
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0
	]), 3, 24);

	// Now create an array of vertex texture coordinates for the cube.
	this.grizzlyTextureCoordBuffer = prepareBuffer(new Float32Array([
		// Front
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Back
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Top
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Bottom
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Right
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Left
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0
	]), 2, 24);

	this.grizzlyVertexIndexBuffer = prepareBuffer(new Uint16Array([
		0,  1,  2,      0,  2,  3,    // front
		4,  5,  6,      4,  6,  7,    // back
		8,  9,  10,     8,  10, 11,   // top
		12, 13, 14,     12, 14, 15,   // bottom
		16, 17, 18,     16, 18, 19,   // right
		20, 21, 22,     20, 22, 23    // left
	]), 1, 36, gl.ELEMENT_ARRAY_BUFFER);


	this.scareFunction = function(x) {
		return Math.pow(x,4);
	}

	this.flyFunction = function(x) {
		return Math.cos(Math.PI * (x - 0.5));
	}


    this.draw = function(pos, scaredProgress) {

        // pos is a position array... [x, y, z]
        // A grizzly can only occupy the bottom row of the scene, so I only have to extract the x coordinate

        mat4.identity(mvMatrix);

        zNorm = 98 - pos[2]*3;

		var xPos = 0.0, yPos = 1.0;
		if(scaredProgress){

			xPos = 10 * this.scareFunction(scaredProgress);
			yPos = 1 + 10 * this.flyFunction(scaredProgress);
		}

        // DEFAULT!! CHANGE IT LATER!

        mat4.translate(mvMatrix, [xPos, yPos, zNorm]);
        mat4.rotate(mvMatrix, degToRad(65), [0, 1, 0]);
        mat4.scale(mvMatrix, [10.0, 10.0, 10.0]);

		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, grizzlyTexture);
		gl.uniform1i(shaderProgram.samplerUniform, 0);

		// Set the vertex positions attribute for the teapot vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, teapotVertexPositionBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, teapotVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Set the texture coordinates attribute for the vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, teapotVertexTextureCoordBuffer);
		gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, teapotVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Set the normals attribute for the vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, teapotVertexNormalBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, teapotVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Set the index for the vertices.
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, teapotVertexIndexBuffer);
		setMatrixUniforms();

		// Draw the teapot
		gl.drawElements(gl.TRIANGLES, teapotVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

    }
}