function TreeObstacle() {
	// Now create an array of vertices for the cube.
	this.treeVertexPositionBuffer = prepareBuffer(new Float32Array([
		// Front face
		-1.0, -1.0,  1.0,
		1.0, -1.0,  1.0,
		1.0,  2.0,  1.0,
		-1.0,  2.0,  1.0,

		// Back face
		-1.0, -1.0, -1.0,
		-1.0,  2.0, -1.0,
		1.0,  2.0, -1.0,
		1.0, -1.0, -1.0,

		// Top face
		-1.0,  2.0, -1.0,
		-1.0,  2.0,  1.0,
		1.0,  2.0,  1.0,
		1.0,  2.0, -1.0,

		// Bottom face
		-1.0, -1.0, -1.0,
		1.0, -1.0, -1.0,
		1.0, -1.0,  1.0,
		-1.0, -1.0,  1.0,

		// Right face
		1.0, -1.0, -1.0,
		1.0,  2.0, -1.0,
		1.0,  2.0,  1.0,
		1.0, -1.0,  1.0,

		// Left face
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,
		-1.0,  2.0,  1.0,
		-1.0,  2.0, -1.0
	]), 3, 24);

	// Now create an array of vertex normals for the cube.
	this.treeVertexNormalBuffer = prepareBuffer(new Float32Array([
		// Front face
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,

		// Back face
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,

		// Top face
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,

		// Bottom face
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,

		// Right face
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,

		// Left face
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0
	]), 3, 24);

	// Now create an array of vertex texture coordinates for the cube.
	this.treeTextureCoordBuffer = prepareBuffer(new Float32Array([
		// Front
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Back
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Top
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Bottom
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Right
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Left
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0
	]), 2, 24);

	this.treeVertexIndexBuffer = prepareBuffer(new Uint16Array([
		0,  1,  2,      0,  2,  3,    // front
		4,  5,  6,      4,  6,  7,    // back
		8,  9,  10,     8,  10, 11,   // top
		12, 13, 14,     12, 14, 15,   // bottom
		16, 17, 18,     16, 18, 19,   // right
		20, 21, 22,     20, 22, 23    // left
	]), 1, 36, gl.ELEMENT_ARRAY_BUFFER);

    this.draw = function(pos) {
        // pos is a position array... [x, y, z]
        // A tree can only occupy the bottom row of the scene, so I only have to extract the x coordinate

        xNorm = (pos[0] - 1) * 5; // Če je 0 vrne -5
        zNorm = 98 - pos[2]*3;

        /*
            DRAW TRUNK
         */
        for (var i = 0; i < 2; i++) {
            mat4.identity(mvMatrix);

            mat4.translate(mvMatrix, [xNorm, 1.0 + i*2.0, zNorm]);

            mvPushMatrix();

			// Draw the cube by binding the array buffer to the cube's vertices
			// array, setting attributes, and pushing it to GL.
			gl.bindBuffer(gl.ARRAY_BUFFER, this.treeVertexPositionBuffer);
			gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.treeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

			// Set the normals attribute for vertices.
			gl.bindBuffer(gl.ARRAY_BUFFER, this.treeVertexNormalBuffer);
			gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.treeVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

			// Set the texture coordinates attribute for the vertices.
			gl.bindBuffer(gl.ARRAY_BUFFER, this.treeTextureCoordBuffer);
			gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.treeTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

			// Activate textures
			gl.activeTexture(gl.TEXTURE0);
			gl.bindTexture(gl.TEXTURE_2D, treeTrunkTexture);
			gl.uniform1i(shaderProgram.samplerUniform, 0);

			// LIGHTING!
			gl.uniform3f(shaderProgram.ambientColorUniform, 0.7,0.7,0.7);

			var lightingDirection = [-0.25, -0.2, -0.2];

			var adjustedLD = vec3.create();
			vec3.normalize(lightingDirection, adjustedLD);
			vec3.scale(adjustedLD, -1);

			gl.uniform3fv(shaderProgram.lightingDirectionUniform, adjustedLD);

			gl.uniform3f(shaderProgram.directionalColorUniform, 0.8,0.8,0.8);

			gl.uniform4fv(shaderProgram.uColor, whiteColor);  // use white color
			gl.bindTexture(gl.TEXTURE_2D, treeTrunkTexture);  // and some texture

			// Draw the cube.
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.treeVertexIndexBuffer);
			setMatrixUniforms();
			gl.drawElements(gl.TRIANGLES, this.treeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

			mvPopMatrix();
        }

        /*
            DRAW TREETOP
         */
        var positions = [
            [xNorm, 4.5, zNorm - 2.0],
            [xNorm - 1.0, 4.5, zNorm],
            [xNorm, 4.5, zNorm + 2.0],
            [xNorm + 1.0, 4.5, zNorm],
            [xNorm, 5.5, zNorm]
        ];

        for (var i = 0; i < positions.length; i++) {
            mat4.identity(mvMatrix);

            mat4.translate(mvMatrix, positions[i]);

            mvPushMatrix();

			// Draw the cube by binding the array buffer to the cube's vertices
			// array, setting attributes, and pushing it to GL.
			gl.bindBuffer(gl.ARRAY_BUFFER, this.treeVertexPositionBuffer);
			gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.treeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

			// Set the normals attribute for vertices.
			gl.bindBuffer(gl.ARRAY_BUFFER, this.treeVertexNormalBuffer);
			gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.treeVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

			// Set the texture coordinates attribute for the vertices.
			gl.bindBuffer(gl.ARRAY_BUFFER, this.treeTextureCoordBuffer);
			gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.treeTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

			// Activate textures
			gl.activeTexture(gl.TEXTURE0);
			gl.bindTexture(gl.TEXTURE_2D, treeTrunkTexture);
			gl.uniform1i(shaderProgram.samplerUniform, 0);

			// LIGHTING!
			gl.uniform3f(shaderProgram.ambientColorUniform, 0.7,0.7,0.7);

			lightingDirection = [-0.25, -0.2, -0.2];

			adjustedLD = vec3.create();
			vec3.normalize(lightingDirection, adjustedLD);
			vec3.scale(adjustedLD, -1);

			gl.uniform3fv(shaderProgram.lightingDirectionUniform, adjustedLD);

			gl.uniform3f(shaderProgram.directionalColorUniform, 0.8,0.8,0.8);

			gl.uniform4fv(shaderProgram.uColor, whiteColor);  // use white color
			gl.bindTexture(gl.TEXTURE_2D, treeTopTexture);  // and some texture

			// Draw the cube.
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.treeVertexIndexBuffer);
			setMatrixUniforms();
			gl.drawElements(gl.TRIANGLES, this.treeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

			mvPopMatrix();
        }
    }
}