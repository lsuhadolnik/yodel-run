function YodlerObject() {
	// Now create an array of vertices for the cube.
	this.yodlerVertexPositionBuffer = prepareBuffer(new Float32Array([
		// Front face
		-1.0, -1.0,  1.0,
		1.0, -1.0,  1.0,
		1.0,  2.0,  1.0,
		-1.0,  2.0,  1.0,

		// Back face
		-1.0, -1.0, -1.0,
		-1.0,  2.0, -1.0,
		1.0,  2.0, -1.0,
		1.0, -1.0, -1.0,

		// Top face
		-1.0,  2.0, -1.0,
		-1.0,  2.0,  1.0,
		1.0,  2.0,  1.0,
		1.0,  2.0, -1.0,

		// Bottom face
		-1.0, -1.0, -1.0,
		1.0, -1.0, -1.0,
		1.0, -1.0,  1.0,
		-1.0, -1.0,  1.0,

		// Right face
		1.0, -1.0, -1.0,
		1.0,  2.0, -1.0,
		1.0,  2.0,  1.0,
		1.0, -1.0,  1.0,

		// Left face
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,
		-1.0,  2.0,  1.0,
		-1.0,  2.0, -1.0
	]), 3, 24);

	// Now create an array of vertex normals for the cube.
	this.yodlerVertexNormalBuffer = prepareBuffer(new Float32Array([
		// Front face
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,

		// Back face
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,

		// Top face
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,

		// Bottom face
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,

		// Right face
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,

		// Left face
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0
	]), 3, 24);

	// Now create an array of vertex texture coordinates for the cube.
	this.yodlerTextureCoordBuffer = prepareBuffer(new Float32Array([
		// Front
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Back
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Top
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Bottom
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Right
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0,
		// Left
		0.0,  0.0,
		1.0,  0.0,
		1.0,  1.0,
		0.0,  1.0
	]), 2, 24);

	this.yodlerVertexIndexBuffer = prepareBuffer(new Uint16Array([
		0,  1,  2,      0,  2,  3,    // front
		4,  5,  6,      4,  6,  7,    // back
		8,  9,  10,     8,  10, 11,   // top
		12, 13, 14,     12, 14, 15,   // bottom
		16, 17, 18,     16, 18, 19,   // right
		20, 21, 22,     20, 22, 23    // left
	]), 1, 36, gl.ELEMENT_ARRAY_BUFFER);
    
    this.draw = function() {
        mat4.identity(mvMatrix);
        
        let xPosition = playerXPosition;
        if (moveInProgress){
            //alert("Move in progress... xPosition:"+xPosition+", moveProgress:"+moveProgress)
            moveProgress += horizontalVelocity;

            xPosition =
                playerXPosition + 
                moveDirection * 
                this.moveCurve(moveProgress) *
                moveDistance;

            if (moveProgress >= 1) {
                moveInProgress = false;
                moveProgress = 0;
                moveDirection = 0;
                
                if(xPosition < -0.1) playerXPosition = -5;
                else if(xPosition > 0.1) playerXPosition = 5;
                else playerXPosition = 0;
            }

        }
        
        mat4.translate(mvMatrix, [xPosition, playerYPosition, 98]);

		var special = false;

        if (jumpInProgress) {
            jumpProgress += verticalVelocity;
            y = this.jumpCurve(jumpProgress) * topOfJumpY;

            mat4.translate(mvMatrix, [0, y, 0]);

            if (jumpProgress >= 1) {
                jumpInProgress = false;
                jumpProgress = 0;
			}
			special = true;
        }

        if(hitStatus.playerHit){

            hitStatus.fallProgress += hitStatus.fallSpeed;
            phi = this.fallCurve(hitStatus.fallProgress) * hitStatus.fallAngle;

            mat4.rotateX(mvMatrix, phi);

            if (hitStatus.fallProgress >= 1) {
                playerCrashed()
            }

			special = true;

        }

        if (slideInProgress){
			mat4.rotateZ(mvMatrix, degToRad(-90));
			
			special = true;
		}
		

		if(!special){
			var elapsed = (new Date().getTime() - startingTime) / 1000;



			mat4.rotateZ(mvMatrix, degToRad(15*Math.sin(Math.PI * (3*elapsed - 0.5))));
			mat4.scale(mvMatrix, [1, 1 + 0.25*Math.sin(Math.PI * (6*elapsed - 0.5)), 1]);

		}
        

        mvPushMatrix();

		// Draw the cube by binding the array buffer to the cube's vertices
		// array, setting attributes, and pushing it to GL.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.yodlerVertexPositionBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.yodlerVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Set the normals attribute for vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.yodlerVertexNormalBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.yodlerVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Set the texture coordinates attribute for the vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.yodlerTextureCoordBuffer);
		gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.yodlerTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

        // Activate textures
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, grassTexture);
        gl.uniform1i(shaderProgram.samplerUniform, 0);

        // LIGHTING!
        gl.uniform3f(shaderProgram.ambientColorUniform, 0.7,0.7,0.7);

        var lightingDirection = [-0.25, -0.2, -0.2];

        var adjustedLD = vec3.create();
        vec3.normalize(lightingDirection, adjustedLD);
        vec3.scale(adjustedLD, -1);

        gl.uniform3fv(shaderProgram.lightingDirectionUniform, adjustedLD);

        gl.uniform3f(shaderProgram.directionalColorUniform, 0.8,0.8,0.8);

		gl.uniform4fv(shaderProgram.uColor, whiteColor);  // use white color
		gl.bindTexture(gl.TEXTURE_2D, grassTexture);  // and some texture

		// Draw the cube.
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.yodlerVertexIndexBuffer);
		setMatrixUniforms();
		gl.drawElements(gl.TRIANGLES, this.yodlerVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

        mvPopMatrix();
    };

    this.jumpCurve = function(x) {
        let min = 0.0, max = 1.0;

        x = Math.min(Math.max(x, min), max);

        // Values must be between 0 and 1

        // Funny one
        // y = -64*(x^2 - x)^3
        // return -64*Math.pow(Math.pow(x,2) - x, 3);

        // Jumpy one
        // y = -(2*x - 1)^2 + 1
        return -Math.pow(2*x - 1, 2) + 1;
    };

    this.moveCurve = function(x) {

        return 1/(1+Math.exp(-(16*(x-0.5))));

    }

    this.fallCurve = function(x) {
		
		//return -Math.pow(2*x + 1, -2) + 1 ;
		return Math.pow(x-1, 7) + 1;


    }
}