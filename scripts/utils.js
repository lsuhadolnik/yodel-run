//
// Matrix utility functions
//
// mvPush   ... push current matrix on matrix stack
// mvPop    ... pop top matrix from stack
// degToRad ... convert degrees to radians
//
function mvPushMatrix() {
    var copy = mat4.create();
    mat4.set(mvMatrix, copy);
    mvMatrixStack.push(copy);
}

function mvPopMatrix() {
    if (mvMatrixStack.length == 0) {
        throw "Invalid popMatrix!";
    }
    mvMatrix = mvMatrixStack.pop();
}

function degToRad(degrees) {
    return degrees * Math.PI / 180;
}

function initTextures() {
    grassTexture = gl.createTexture();
    grassTexture.image = new Image();
    grassTexture.image.onload = function () {
        handleTextureLoaded(grassTexture);
    };
    grassTexture.image.src = "./assets/textures/grass.gif";

    pathTexture = gl.createTexture();
    pathTexture.image = new Image();
    pathTexture.image.onload = function () {
        handleTextureLoaded(pathTexture);
    };
    pathTexture.image.src = "./assets/textures/path.gif";

	cloudTexture = gl.createTexture();
	cloudTexture.image = new Image();
	cloudTexture.image.onload = function () {
		handleTextureLoaded(cloudTexture);
	};
	cloudTexture.image.src = "./assets/textures/cloud.gif";

	rockTexture = gl.createTexture();
	rockTexture.image = new Image();
	rockTexture.image.onload = function () {
		handleTextureLoaded(rockTexture);
	};
	rockTexture.image.src = "./assets/textures/rock.gif";

	treeTrunkTexture = gl.createTexture();
	treeTrunkTexture.image = new Image();
	treeTrunkTexture.image.onload = function () {
		handleTextureLoaded(treeTrunkTexture);
	};
	treeTrunkTexture.image.src = "./assets/textures/treeTrunk.gif";

	treeTopTexture = gl.createTexture();
	treeTopTexture.image = new Image();
	treeTopTexture.image.onload = function () {
		handleTextureLoaded(treeTopTexture);
	};
	treeTopTexture.image.src = "./assets/textures/treeTop.gif";

	branchTexture = gl.createTexture();
	branchTexture.image = new Image();
	branchTexture.image.onload = function () {
		handleTextureLoaded(branchTexture);
	};
	branchTexture.image.src = "./assets/textures/branch.gif";

	overhangTexture = gl.createTexture();
	overhangTexture.image = new Image();
	overhangTexture.image.onload = function () {
		handleTextureLoaded(overhangTexture);
	};
	overhangTexture.image.src = "./assets/textures/overhang.gif";

	grizzlyTexture = gl.createTexture();
	grizzlyTexture.image = new Image();
	grizzlyTexture.image.onload = function () {
		handleTextureLoaded(grizzlyTexture);
	};
	grizzlyTexture.image.src = "./assets/textures/grizzly.gif";

	goldTexture = gl.createTexture();
	goldTexture.image = new Image();
	goldTexture.image.onload = function () {
		handleTextureLoaded(goldTexture);
	};
	goldTexture.image.src = "./assets/textures/gold.gif";
}

function handleTextureLoaded(texture) {
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

    // Third texture usus Linear interpolation approximation with nearest Mipmap selection
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);

    gl.bindTexture(gl.TEXTURE_2D, null);

    // when texture loading is finished we can draw scene.
    texturesLoaded += 1;
}

function initBuffers() {
	whiteTexture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, whiteTexture);
	whitePixel = new Uint8Array([255, 255, 255, 255]);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, whitePixel);
}

function loadJSONModels() {
	loadWolf();
	loadCoin();
}

function loadWolf() {
	var request = new XMLHttpRequest();
	request.open("GET", "./assets/models/Wolf.json");
	request.onreadystatechange = function () {
		if (request.readyState == 4) {
			handleLoadedWolf(JSON.parse(request.responseText));
		}
	};
	request.send();
}

function handleLoadedWolf(wolfData) {
	// Pass the normals into WebGL
	teapotVertexNormalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, teapotVertexNormalBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(wolfData.vertexNormals), gl.STATIC_DRAW);
	teapotVertexNormalBuffer.itemSize = 3;
	teapotVertexNormalBuffer.numItems = wolfData.vertexNormals.length / 3;

	// Pass the texture coordinates into WebGL
	teapotVertexTextureCoordBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, teapotVertexTextureCoordBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(wolfData.vertexTextureCoords), gl.STATIC_DRAW);
	teapotVertexTextureCoordBuffer.itemSize = 2;
	teapotVertexTextureCoordBuffer.numItems = wolfData.vertexTextureCoords.length / 2;

	// Pass the vertex positions into WebGL
	teapotVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, teapotVertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(wolfData.vertexPositions), gl.STATIC_DRAW);
	teapotVertexPositionBuffer.itemSize = 3;
	teapotVertexPositionBuffer.numItems = wolfData.vertexPositions.length / 3;

	// Pass the indices into WebGL
	teapotVertexIndexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, teapotVertexIndexBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(wolfData.indices), gl.STATIC_DRAW);
	teapotVertexIndexBuffer.itemSize = 1;
	teapotVertexIndexBuffer.numItems = wolfData.indices.length;
}

function loadCoin() {
	var request = new XMLHttpRequest();
	request.open("GET", "./assets/models/Coin.json");
	request.onreadystatechange = function () {
		if (request.readyState == 4) {
			handleLoadedCoin(JSON.parse(request.responseText));
		}
	};
	request.send();
}

function handleLoadedCoin(coinData) {
	// Pass the normals into WebGL
	coinVertexNormalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, coinVertexNormalBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(coinData.vertexNormals), gl.STATIC_DRAW);
	coinVertexNormalBuffer.itemSize = 3;
	coinVertexNormalBuffer.numItems = coinData.vertexNormals.length / 3;

	// Pass the texture coordinates into WebGL
	coinVertexTextureCoordBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, coinVertexTextureCoordBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(coinData.vertexTextureCoords), gl.STATIC_DRAW);
	coinVertexTextureCoordBuffer.itemSize = 2;
	coinVertexTextureCoordBuffer.numItems = coinData.vertexTextureCoords.length / 2;

	// Pass the vertex positions into WebGL
	coinVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, coinVertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(coinData.vertexPositions), gl.STATIC_DRAW);
	coinVertexPositionBuffer.itemSize = 3;
	coinVertexPositionBuffer.numItems = coinData.vertexPositions.length / 3;

	// Pass the indices into WebGL
	coinVertexIndexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, coinVertexIndexBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(coinData.indices), gl.STATIC_DRAW);
	coinVertexIndexBuffer.itemSize = 1;
	coinVertexIndexBuffer.numItems = coinData.indices.length;
}