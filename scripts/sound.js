
  var soundDisabled = false;
  var soundReady = false;

  function soundLoaded() {
    soundReady = true;
  }
  
  function playGameSound() {
  
    if(soundDisabled) return;

    sound.stop(1);
    sound.play(1,"gameplay");
    sound.loop(1);
  
  }
  
  function pauseSound() {
  
    if(soundDisabled) return;

   sound.pause(1);
  
  }
  
  function yodelSound() {

    if(soundDisabled) return;

  
    sound.play("yodel", "yodel");
  
  }
  
  function jumpSound() {

    if(soundDisabled) return;
  
  sound.play("jump", "jump");
  
  }

  function coinsound() {

    if(soundDisabled) return;
  
    sound.play("coin", "coin");
  
  }
  
  function playerCrashedSound() {

    if(soundDisabled) return;
  
    sound.stop(1);
    sound.play("crash", "crash");
    sound.play(1, "pause", 0, 5);
    sound.loop(1);
  
  }
  
  function toggleSound() {

    if(soundDisabled) {
      soundDisabled = false
    }

    else {
      soundDisabled = true
      sound.stop(1);
    }
    
  }
  
  
  
  var sound = (function(soundReady) {
  
    var context;
  
    var sounds = {
      gameplay: {
        file: "assets/gameplay.mp3",
        name: "gameplay"
      },
      pause: {
        file: "assets/pause.mp3",
        name: "pause"
      },
      jump: {
        file: "assets/jump.mp3",
        name: "jump"
      },
      yodel: {
        file: "assets/yodel.mp3",
        name: "yodel"
      },
      crash: {
        file: "assets/crash.mp3",
        name: "crash"
      },
      coin: {
        file: "assets/coin.mp3",
        name: "coin"
      }
    }
  
    var counter = (function(i, then) {
      var cnt = i;
  
      return function() 
      {
          cnt--;
          if(cnt == 0){
            then();
          }
          return cnt;
      }
  
  });
  
    // Init a request counter. When all requests are processed and done, soundReady is called.
    var requestFinished = counter(
      Object.keys(sounds).length, 
      soundReady
    );
  
    var loadSound = function(sound) {
  
      console.log("Loading "+sound.name)
  
      var request = new XMLHttpRequest();
      request.open('GET', sound.file, true);
      request.responseType = 'arraybuffer';
  
      // Decode asynchronously
      request.onload = function() {
        context.decodeAudioData(request.response, function(buffer) {
  
          sound.buffer = buffer;
          sound.loaded = true;
  
          // OMG So beautiful!!
          requestFinished();
  
        }, function(err) {
            alert("DECODING ERROR! "+sound.name);
            console.log(err);
        });
      }
  
      request.onerror = function(ev) {
          alert("Request for sound "+sound.name+" failed with error "+ ev);
          console.log(ev);
      }
      request.send();
    }
  
    try {
      // Fix up for prefixing
      window.AudioContext = window.AudioContext||window.webkitAudioContext;
      context = new AudioContext();
    }
    catch(e) {
      alert('Web Audio API is not supported in this browser');
    }
  
    for(var sound in sounds){
      loadSound(sounds[sound]);
    }
  
    var sessions = {};
  
    return {
      loop: function(sessionName) {
  
        var doLoop = sessions[sessionName].loop ? true : false;
        sessions[sessionName].loop = !doLoop;
  
      },

      play: function(sessionName, soundName, offset, startAfter){
  
        if(!startAfter){ startAfter = 0; }
        if(!offset){ offset = 0; }
  
        var sound = sounds[soundName];
  
        var source = context.createBufferSource();
        source.buffer = sound.buffer;
        source.connect(context.destination);
  
        sessions[sessionName] = source;
        sessions[sessionName].startedAt = context.currentTime - offset + startAfter;
        sessions[sessionName].soundName = soundName;
        
        if(offset)
          source.start(context.currentTime + startAfter, offset);
        else
          source.start(context.currentTime + startAfter);
  
      },
  
      pause: function(sessionName) {
          
        if(sessions[sessionName]){
          var session = sessions[sessionName];
          if(session.pausedAt){
            var offset = context.currentTime - session.startedAt;
            
            this.play(sessionName, session.soundName, offset, 0);
            
          }
          else {
            session.pausedAt = context.currentTime - session.startedAt;
            session.stop();
          }
        } 
          
      },
  
      
  
      stop: function(sessionName){
        if(sessions[sessionName]){
          sessions[sessionName].stop();
          sessions[sessionName] = null
        }
          
      }
    }
  
  
  })(function(){soundLoaded()});