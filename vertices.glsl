// atributes for setting vertex position, normals and texture coordinates
attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;
attribute vec2 aTextureCoord;

uniform mat4 uMVMatrix;	// model-view matrix
uniform mat4 uPMatrix;	// projection matrix
uniform mat3 uNMatrix;	// normal matrix

uniform vec3 uAmbientColor;	// ambient color uniform

uniform vec3 uLightingDirection;	// light direction uniform
uniform vec3 uDirectionalColor;		// light color

uniform bool uUseLighting;	// lighting switch

// variable for passing texture coordinates and lighting weights
// from vertex shader to fragment shader
varying vec2 vTextureCoord;
varying vec3 vLightWeighting;

void main(void) {
    // calculate the vertex position
    gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
    vTextureCoord = aTextureCoord;

    // transform normals
    vec3 transformedNormal = uNMatrix * aVertexNormal;

    // calculate weight for directional light
    float directionalLightWeighting = max(dot(transformedNormal, uLightingDirection), 0.0);

    // calculate lighting
    vLightWeighting = uAmbientColor + uDirectionalColor * directionalLightWeighting;
}