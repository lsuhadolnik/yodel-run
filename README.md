# YodelRun

YodelRun - yet another runner game, this time with a yodler as a main character.

# TODO

[TODO] Dokumentacija

[TODO] Naraščanje hitrosti igre čez čas

[TODO] [Maybe] Kovanci

[Maybe] On-Screen controls za igranje na telefonu




### Done
[TODO] Skok s pospeškom, da je bolj realistično...

[DONE] Zdej je treba nardit še razrede za ostale ovire.

[DONE] Animacija in logika za crouch

[DONE] Treba je nardit zaznavo trkov

[DONE] Treba je močno izlepšat izgled

[DONE] Treba je realizirat izris preko generatorja.

[DONE] Treba je nardit, da se vse skupi premika proti jodlerju

[DONE] Lightning - Phong ali Lambert lightning - prekopirat iz vaj...

[DONE] Treba je nardit Audio Engine

[DONE] Smooth move

[DONE] Import 3D modelov

[DONE] 3D model za medveda / volka

[DONE] Teksture

[DONE] Ovire se začnejo generirat nekaj segmentov naprej od igralca, da se ne zabije takoj


# Struktura kode

Skripte uporabljajo jQuery

- index.html
Vstopna stran

- style.css
Vsebuje stil uporabniškega vmesnika.

- script.js
Prva skripta, ki se zažene. Kliče metode iz drugih skript.

- globals.js
Vsebuje vse globalne spremenljivke aplikacije.

- bootstrap.js
Vsebuje metode, ki omogočajo izpis. WebGL stuff, ene stvari bo še treba prenest drugam.

- events.js
Vsebuje event handlerje za dogodke, ki spremenijo GUI

- utils.js
Pomagalne metode, ki se jih rabi po aplikaciji



# Kako deluje?

Ko se dokument naloži (body `onload`), se pokliče metoda `start()` (*script.js*). Ta pokliče ustrezne metode, ki inicializirajo WebGL context (v *bootstrap.js*). 

Na začetku je viden overlay, ki je `div` v *index.html*. Ta ima gumb start, ki ob kliku pokliče metodo `startGame()` v *events.js*. Ta pokliče metodo `startDrawing()` v *script.js*, ki zažene izris - požene glavno zanko aplikacije, omogoči zajem tipkovnice...

Ko se player zaleti, pokliči metodo `playerCrashed()` (*events.js*), ki poskrbi, da se ustavi izris, pokaže overlay, posodobi točke...

Točke posodobi z metodo `setScore()` (*events.js*), ki tudi posodobi uporabniški vmesnik.

Ko razvijaš igro, si na konec metode `start()` dodaj klic `startGame()`, da se ti stalno ne prikazujejo okna...


Uporaba:
Najprej inicializiraj objekt:
```javascript
var yodler = new YodlerObject();
```
Ta klic poskrbi za ustrezne bufferje in ta jajca, zato da ti ni treba tega na dolgo pisat zmeri.
Ko hočeš narisat jodlerja, sam napišeš
```javascript
yodler.draw()
```
To metodo enostavno daš v funkcijo `drawScene()` in se ti lepo izrisuje ob vsakem ciklu.

Enako deluje tudi `scene.js`, ki izriše kocko, v kateri se nahajaš. Tole bo treba verjetno malo predelati, da bo posebej samo cesta in posebej okoliški teren, da bomo lohk še kake druge objekte tam rendral...

Potem je pa še `rock.js`, ki vsebuje objekt `RockObstacle`, ki izriše skalo (zanekrat samo kocko :) ). Njegova draw metoda izgleda tako:
```javascript
this.draw = function(pos) {...}
```

`pos` je array položaja skale -> `[x, y, z]`. 
Potem ko objekt inicializiraš, skalo narišeš tako: 
```javascript
function drawScene() {
    ...

    rock.draw([0.0, 1.0, 1.0]);

    ...
}
```

Koordinate so normalizirane Če je generator vrnil 
```
Segment od Yodlerja oddaljen 12 segmentov:
-----------
| 0  0  0 |
| 0  0  0 |
| 1  1  0 |
-----------
```
se vsako od skal dat izrisat kot 
```javascript
rock.draw([0, 0, 12]);
rock.draw([1, 0, 12]);
```

