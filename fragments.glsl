precision mediump float;

// uniform attribute for setting texture coordinates
varying vec2 vTextureCoord;
// uniform attribute for setting lighting
varying vec3 vLightWeighting;

// uniform attribute for setting 2D sampler
uniform sampler2D uSampler;
// uniform for color
uniform vec4 uColor;

void main(void) {
    // sample the fragment color from texture
    //vec4 textureColor = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t));
    //gl_FragColor = vec4(textureColor.rgb * vLightWeighting, textureColor.a);

    vec4 textureColor = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t));
    gl_FragColor = vec4(textureColor.rgb * vLightWeighting, textureColor.a) * uColor;
}